﻿


using System.Collections.Generic;

public static class EnumeratorExtensions {

    public static T Random<T>(this IList<T> enumerable) { 
        return enumerable[UnityEngine.Random.Range(0, enumerable.Count)];
    }
}