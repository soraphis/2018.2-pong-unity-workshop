﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class BallParticleMovement : MonoBehaviour {
	[SerializeField] private Transform ball;
	private new ParticleSystem particleSystem;
	
	
	private void Start() {
		particleSystem = GetComponent<ParticleSystem>();
		var particleSystemEmission = particleSystem.emission;
		particleSystemEmission.enabled = false;
	}
	
	void LateUpdate () { this.transform.position = ball.transform.position; }
}
