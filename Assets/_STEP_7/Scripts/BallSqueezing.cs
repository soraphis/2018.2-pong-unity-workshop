﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSqueezing : MonoBehaviour {
	private new Rigidbody2D rigidbody;

	[SerializeField][Range(50, 800)] private float speedFaktor = 100;
	
	void Awake() { rigidbody = GetComponent<Rigidbody2D>(); }

	void LateUpdate() {
		this.transform.up = rigidbody.velocity.normalized;

		var sqz = Mathf.Lerp(1, 0.2f, rigidbody.velocity.sqrMagnitude / speedFaktor);
		this.transform.localScale = 0.18f * new Vector3(sqz, 1, 1);
	}
	
}
