﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace _STEP_7.Scripts {
    public class BallMovement : MonoBehaviour {
        private new Rigidbody2D rigidbody;

        private void Awake() { rigidbody = GetComponent<Rigidbody2D>(); }

        //[Header("debug only")]
        //private Vector3 oldPos;
        //private float speed;
        
        private static readonly Vector2[] axiss = new[] {Vector2.up, Vector2.right};
        
        private void FixedUpdate() {
            for (var i = 0; i < axiss.Length; ++i) {
                var axis = axiss[i];
                if (Mathf.Abs(Vector2.Dot(rigidbody.velocity, axis)) < 0.0625f) {
                    // var d = Random.Range(0, 2) * 2 - 1;
                    
                    rigidbody.AddForce(transform.position.normalized * -10f);
                }
            }

            
            rigidbody.velocity = rigidbody.velocity.normalized * Mathf.Min(rigidbody.velocity.magnitude, 20f);


        }

        private void LateUpdate() {
            if (transform.position.x < -10 || transform.position.x > 10
            ||  transform.position.y < -10 || transform.position.y > 10) {
                GameManager.Instance.RestartBall();
                Debug.LogWarning("Ball Destroyed because out of bounds!" + transform.position);
            }

            //speed = (transform.position - oldPos).magnitude * Time.deltaTime;
            //oldPos = transform.position;
        }
    }
}