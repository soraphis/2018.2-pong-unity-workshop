﻿using System.Collections;
using System.Collections.Generic;
using STEP_3;
using STEP_9.Scripts;
using UnityEngine;

public class AI_InputProvider : InputReceiver {

    [SerializeField] private Transform ball;
    [SerializeField] private Transform character;

    void Start() {
        ball = ball ?? BallMovement.instance.transform;
        character = character ?? this.transform;
    }
    
    void Update() {
        if(! ball.gameObject.activeSelf) return;
        UpDownMovement = Mathf.RoundToInt(Mathf.Clamp(ball.position.y - character.position.y, -1, 1));
    }
    
}
