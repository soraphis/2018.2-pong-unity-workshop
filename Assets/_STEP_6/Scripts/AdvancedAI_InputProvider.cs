﻿using STEP_3;
using STEP_9.Scripts;
using UnityEngine;

public class AdvancedAI_InputProvider : InputReceiver {

    [SerializeField] private Transform ball;
    [SerializeField] private Transform character;
    
    // ReSharper disable once Unity.NoNullCoalescing
    void Start() {
        ball = ball ?? BallMovement.instance.transform;
        character = character ?? this.transform;
    }
    
    void Update() {
        if(! ball.gameObject.activeSelf) return;

        var x = character.transform.position.x;


        var b = ball.transform.position;
        var bf = ball.transform.up; // this is forward for 2d
        
         
        // var t = (x - b.x) / bf.x; // b + t * bf = x <=> (x - b)/bf 
        // if (t < 0) return;

        int i = 0; 
        do {

            RaycastHit2D hit = Physics2D.Raycast(b, bf, 100, LayerMask.GetMask(new[] {"World", "ScoreZone"}));
            b = hit.point;
            bf = 2 * (Vector2.Dot(-bf, hit.normal)) * hit.normal + (Vector2)bf;

        } while (Mathf.Abs(b.x) < Mathf.Abs(x) && ++i < 15);
        
        UpDownMovement = Mathf.RoundToInt(Mathf.Clamp(b.y - character.transform.position.y, -1, 1));
    }
    
}
