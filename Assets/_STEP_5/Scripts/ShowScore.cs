﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace STEP_5 {

    public class ShowScore : MonoBehaviour {

        [SerializeField] private TextMeshProUGUI[] ScoreText;

        void Update() {
            ScoreText[0].text = GameManager.Instance.Score[0].ToString();
            ScoreText[1].text = GameManager.Instance.Score[1].ToString();
        }
    }
    
}


