﻿using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour, ISerializationCallbackReceiver{

	public static GameManager Instance { get; private set; } // extreme simple Singleton Pattern.

	[SerializeField] private Rigidbody2D ball; 
	
	private int[] score = new[] {0, 0};
	public int[] Score { get { return score; } }
	private int lastScore = -1; // no player scored last

	private void Awake() { Instance = this; }

	IEnumerator Start () {
		ball.gameObject.SetActive(false);
		yield return new WaitForSeconds(1.3f);
		
		ball.gameObject.SetActive(true);
		ball.transform.position = Vector3.zero;
		ball.velocity = Vector2.zero;
		
		// initial ball direction:
		int d = lastScore == -1 ? Random.Range(0, 2) : lastScore; // Random.Range is exclusive max
		d = d * 2 - 1; // from [0, 1] -> [-1, 1];
		
		float f = Random.Range(-45f, 45f);

		var v = Quaternion.AngleAxis(f, Vector3.forward) * new Vector3(d, 0, 0);
		ball.AddForce(v * Random.Range(3f, 5f), ForceMode2D.Impulse);
	}

	/// <param name="player">0 is left player, 1 is right player</param>
	public void AddScoreForPlayer(int player) {
		++score[player];
		lastScore = player;
		RestartBall();
	}

	public void RestartBall() {
		StartCoroutine(Start());		
	}


	
	public void OnBeforeSerialize() {  }

	public void OnAfterDeserialize() { Instance = this; }
}
