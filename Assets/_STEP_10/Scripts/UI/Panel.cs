﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Image))]
[SelectionBase]
public class Panel : MonoBehaviour {

	[SerializeField] protected AudioSource buttonSound;
	[SerializeField] protected AudioSource backSound;
	private Panel previousPanel;
	public static Panel activePanel; // added in STEP_11
	
	void Start() {
		this.transform.localPosition = Vector3.zero;
		if (this.transform.GetSiblingIndex() != 0) gameObject.SetActive(false);
		else activePanel = this; // added in STEP_11
	}

	private void OnEnable() { activePanel = this; }


	public void Back() {
		if(previousPanel == null) return;
		previousPanel.gameObject.SetActive(true);
		
		var t = this.transform as RectTransform;
		
		if(backSound != null) backSound.Play();
		
		t.DOLocalMoveX(t.rect.width, 0.5f).SetEase(Ease.OutCubic).OnComplete(() => {
			previousPanel.transform.SetAsLastSibling();
			gameObject.SetActive(false);
			t.localPosition = Vector3.zero;
		});
	}
	
	public void Transist(Panel other) {
		other.previousPanel = this;
		
		if(buttonSound != null) buttonSound.Play();
		
		var t = other.transform as RectTransform;
		t.localPosition = Vector3.zero;
		other.gameObject.SetActive(true);
		t.SetAsLastSibling();
		t.DOLocalMoveY(-t.rect.height, 0.6f).SetEase(Ease.OutBounce).From().OnComplete(() => gameObject.SetActive(false));
	}
}
