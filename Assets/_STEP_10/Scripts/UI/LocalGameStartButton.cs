﻿using System;
using System.Collections.Generic;
using STEP_3;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LocalGameStartButton : MonoBehaviour {

    [SerializeField] private TMP_Dropdown[] dropdowns;

    [SerializeField] private int[] scenesToLoad;
    
    Dictionary<int, Type> inputChoices = new Dictionary<int, Type>() {
        {0, typeof(Axis_InputProvider)},
        {1, typeof(Axis_InputProvider)},
        {2, typeof(AI_InputProvider)},
        {3, typeof(AdvancedAI_InputProvider)},
    };

    private void OnValidate() {
        if(dropdowns == null || dropdowns.Length != 2)
            dropdowns = new TMP_Dropdown[2];
    }


    public void StartGame() {
        DontDestroyOnLoad(this.gameObject); 
        for (var i = 0; i < dropdowns.Length; i++) {
            DontDestroyOnLoad(dropdowns[i].gameObject);
        }
        SceneManager.sceneLoaded += OnSceneLoaded;
        
        SceneManager.LoadScene(scenesToLoad[0], LoadSceneMode.Single);
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1) {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        for (int i = 1; i < scenesToLoad.Length; ++i) {
            SceneManager.LoadScene(scenesToLoad[i], LoadSceneMode.Additive);            
        }

        string[] playerObjectNames = {"Player Left", "Player Right"};
        
        for (int i = 0; i < 2; ++i) { // for both players
            var mov = GameObject.Find(playerObjectNames[i]).GetComponent<MovementController>();
            var con = mov.gameObject.AddComponent(inputChoices[dropdowns[i].value]) as InputReceiver;
            if (dropdowns[i].value == 0) { // [W] / [S]
                (con as Axis_InputProvider).axisName = "Vertical";
            }else if (dropdowns[i].value == 1) { // up/down
                (con as Axis_InputProvider).axisName = "Vertical_2";
            }
            
            
            mov.input = con;
        }
        Debug.Log("1");
        
        for (var i = 0; i < dropdowns.Length; i++) {
            if(dropdowns[i] != null) Destroy(dropdowns[i].gameObject);
        }     
        if(this != null)  Destroy(this.gameObject);
    }
}