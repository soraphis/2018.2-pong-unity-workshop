﻿using UnityEngine;

namespace STEP_3 {
    public class MovementController : MonoBehaviour {

        public InputReceiver input; // soll später von außen gefüllt werden
        [SerializeField] private float bounds = 10; // default not too low
        [SerializeField] private float speed = 1;

        private void OnValidate() { bounds = Mathf.Max(0, bounds); }

        void Update() {
            this.transform.position += speed * Time.deltaTime * input.UpDownMovement * Vector3.up;

            var y = Mathf.Clamp(this.transform.position.y, -bounds, bounds);
            this.transform.position = new Vector3(this.transform.position.x, y, 0);
        }

    }
}