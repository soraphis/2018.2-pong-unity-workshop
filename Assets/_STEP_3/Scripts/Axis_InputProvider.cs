﻿using UnityEngine;

namespace STEP_3 {

    public class Axis_InputProvider : InputReceiver {

        public string axisName = "Vertical";

        private void Update() {
            this.UpDownMovement = Input.GetAxisRaw(axisName);
        }
    }

}
