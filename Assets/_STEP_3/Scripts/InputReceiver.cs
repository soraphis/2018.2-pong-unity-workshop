﻿using UnityEngine;


/* Kein Interface, weil unity interfaces nicht serialisiert  */
namespace STEP_3 {
    public abstract class InputReceiver : MonoBehaviour {
        public float UpDownMovement { get; protected set; }
    }
}
