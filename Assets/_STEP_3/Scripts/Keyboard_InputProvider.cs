﻿using UnityEngine;


namespace STEP_3 {
    public class Keyboard_InputProvider : InputReceiver {

        private void Update() {
            float f = 0;
            f += Input.GetKey(KeyCode.UpArrow) ? 1 : 0;
            f += Input.GetKey(KeyCode.DownArrow) ? -1 : 0;

            this.UpDownMovement = f;
        }
    }
}