﻿using System;
using System.Collections;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace STEP_9 {

public class GameManager : MonoBehaviour, ISerializationCallbackReceiver {

	private static GameManager _instance;
	public static GameManager Instance { get; private set; } // extreme simple Singleton Pattern.

	[SerializeField] private Rigidbody2D ball; 
	
	private int[] score = new[] {0, 0};
	public int[] Score { get { return score; } }
	private int lastScore = -1; // no player scored last

	private void Awake() { Instance = this; }

	[HideInInspector] public ShowScore scoreHUD = null; // this is filled from the UI 
	
	IEnumerator Start () {
		ball.gameObject.SetActive(false);
		yield return new WaitForSeconds(1.3f);
		
		ball.gameObject.SetActive(true);
		ball.transform.position = Vector3.zero;
		ball.velocity = Vector2.zero;
		
		// initial ball direction:
		int d = lastScore == -1 ? Random.Range(0, 2) : lastScore; // Random.Range is exclusive max
		d = d * 2 - 1; // from [0, 1] -> [-1, 1];
		
		float f = Random.Range(-45f, 45f);

		var v = Quaternion.AngleAxis(f, Vector3.forward) * new Vector3(d, 0, 0);
		ball.AddForce(v * Random.Range(3f, 5f), ForceMode2D.Impulse);
	}

	/// <param name="player">0 is left player, 1 is right player</param>
	public void AddScoreForPlayer(int player) {
		++score[player];
		lastScore = player;
		ball.gameObject.SetActive(false);

		const int MIN_SCORE_TO_WIN = 11;
		const int MIN_SCORE_DISTANCE_TO_LOOSER = 2;

		var dist = Mathf.Abs(score[0] - score[1]);
		if (score[player] >= MIN_SCORE_TO_WIN && dist >= MIN_SCORE_DISTANCE_TO_LOOSER) {
			// win player
			StartCoroutine(ShowMatchOver(player));
			return;
		}

		if (score.Any(s => s + 1 >= MIN_SCORE_TO_WIN) && dist >= 1) {
			// matchball!
			StartCoroutine(ShowMatchball());
			return;
		}
		
		RestartBall();
	}

	IEnumerator ShowMatchOver(int player) {
		scoreHUD.scoreText[0].rectTransform.DOScale(0.4f, 0.3f);
		scoreHUD.scoreText[1].rectTransform.DOScale(0.4f, 0.3f);
		scoreHUD.winScreen.gameObject.SetActive(true);
		scoreHUD.winText.text = string.Format("PLAYER {0} WINS", player+1);

		yield return scoreHUD.matchballSign.DOScale(0, 1).From().WaitForCompletion();
	}

	IEnumerator ShowMatchball() {
		scoreHUD.scoreText[0].rectTransform.DOScale(0.4f, 0.3f);
		scoreHUD.scoreText[1].rectTransform.DOScale(0.4f, 0.3f);
		scoreHUD.matchballSign.gameObject.SetActive(true);

		yield return scoreHUD.matchballSign.DOScale(0, 1).From().WaitForCompletion();
		
		scoreHUD.scoreText[0].rectTransform.DOScale(1f, 0.3f);
		scoreHUD.scoreText[1].rectTransform.DOScale(1f, 0.3f);
		scoreHUD.matchballSign.gameObject.SetActive(false);
		yield return Start();
	}
	
	public void RestartBall() {
		StartCoroutine(Start());
	}
	
	public void RestartGame() {
		score[0] = 0;
		score[1] = 0;
		lastScore = -1;
		scoreHUD.winScreen.gameObject.SetActive(false);
		scoreHUD.scoreText[0].rectTransform.localScale = Vector3.one;
		scoreHUD.scoreText[1].rectTransform.localScale = Vector3.one;
		StartCoroutine(Start());
	}

	
	public void OnBeforeSerialize() {  }

	public void OnAfterDeserialize() { Instance = this; }
}


}