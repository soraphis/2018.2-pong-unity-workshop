﻿using System;
using UnityEngine;

namespace STEP_9.Scripts {
    public class Score : MonoBehaviour {

        [SerializeField] private Collider2D[] scoreZone;

        private void OnTriggerEnter2D(Collider2D other) {
            var i = 1 - Array.IndexOf(scoreZone, other);
            if(i == 2) return;
            GameManager.Instance.AddScoreForPlayer(i);
            // Debug.Log("SCORE! for player " + (i));
        }
    }
}