﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STEP_9 {
public class BallHitEffects : MonoBehaviour {


	[SerializeField][Range(0, 100)] private int amount = 10;
	[SerializeField]private new ParticleSystem particleSystem;
	
	private new Camera camera;
	
	private void Start() {
		camera = Camera.main;
	}

	private IEnumerator ScreenShake(float strenght = 0.125f, int frames = 2) {
		var start = camera.transform.position;
		
		// float alpha = Random.Range(0, 180);
		// float beta = Random.Range(-15, 15);

		for (int i = 0; i < frames; ++i) {
			var pos = start + Random.insideUnitSphere * strenght;
			camera.transform.position = pos;
			yield return null;
		}
		yield return null;
		camera.transform.position = start;
	}
	
	private void OnCollisionEnter2D(Collision2D col) {
		if(col.gameObject.layer == 10 /* player */){ // TODO: change this line, you know how!
			if(this.gameObject.activeSelf) StartCoroutine(ScreenShake());
		} else {
			// hit world!
			particleSystem.Emit(amount);
			
		}
	}

	private void OnTriggerEnter2D() {
		GameManager.Instance.StartCoroutine(ScreenShake(strenght:0.3f, frames:7));
		particleSystem.Emit(amount * 3);
	}
}
}