﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace STEP_9 {

public class ShowScore : MonoBehaviour {

	public RectTransform matchballSign;
	public RectTransform winScreen;
	public TextMeshProUGUI winText;
	public TextMeshProUGUI[] scoreText;

	void Start() {
		GameManager.Instance.scoreHUD = this;
		
	}
	
	void Update() {
		scoreText[0].text = GameManager.Instance.Score[0].ToString();
		scoreText[1].text = GameManager.Instance.Score[1].ToString();
	}

	public void RestartButton() {
		GameManager.Instance.RestartGame();
	}	
	
	public void MainMenuButton() {
		SceneManager.LoadScene(0);
	}
	
}

}