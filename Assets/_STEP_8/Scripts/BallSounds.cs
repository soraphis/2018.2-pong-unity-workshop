﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BallSounds : MonoBehaviour {
	//private Rigidbody2D rigidbody;

	[SerializeField][Range(50, 800)] private float speedFaktor = 100;
	[SerializeField]private AudioSource wuschSource;
	[SerializeField]private List<AudioClip> clips = new List<AudioClip>();
	[SerializeField] private AudioClip boomClip;

	void Awake() {
		//rigidbody = GetComponent<Rigidbody2D>();
	}


	void LateUpdate() {
		var vv = GetComponent<Rigidbody2D>().velocity.sqrMagnitude;

		wuschSource.volume = Mathf.Lerp(0.2f, 1f, vv / speedFaktor);
		wuschSource.pitch = Mathf.Lerp(0.8f, 3, vv / speedFaktor);
	}


	private void OnCollisionEnter2D(Collision2D col) {
		if(clips.Count > 0) AudioSource.PlayClipAtPoint(clips.Random(), this.transform.position, 1);
	}

	private void OnTriggerEnter2D() {
		AudioSource.PlayClipAtPoint(boomClip, this.transform.position, 2);
	}
}
